let courses = [

	{
		id: "fil101",
		name: "Filipino",
		description: "lalala",
		price: 100,
		isActive: true
	},
	{
		id: "eng101",
		name: "English",
		description: "lalala",
		price: 120,
		isActive: true
	},
	{
		id: "math101",
		name: "Mathematics",
		description: "lalala",
		price: 140,
		isActive: true
	},
	{
		id: "his101",
		name: "History",
		description: "lalala",
		price: 160,
		isActive: true
	}
];

console.log(courses);

let {id, name, description, price, isActive} = courses

//addCourse

let addCourse = (inId, inName, inDesc, inPrice, inIsActive) => {
			
			courses.push({
			id: inId,
			name: inName, 
			description: inDesc, 
			price: inPrice, 
			isActive: inIsActive
		});

		alert(`You have created the course ${inName}. Its price is ${inPrice}.`)
		console.log(`You have created the course ${inName}. Its price is ${inPrice}.`)
	}

addCourse("span101","Spanish","lalala", 180, true);
console.log(courses);


//archiveCourse

let archiveCourse = (change = prompt(`Which course/name do you want to update?`)) => {
	let courseIndex = courses.findIndex(course => course.name == change);
	if(courses[courseIndex] !== undefined){
		console.log("Before update: ", courses[courseIndex])
		courses[courseIndex].isActive = false
		console.log("After update: ", courses[courseIndex])
	} else {	
		alert(`Course/name is not listed in this array.`)
	}
}

archiveCourse();


//deleteCourse

console.log(courses);

let deleteCourse = (answer = prompt(`Do you want the last course to be deleted? y/n`)) => {
	if( answer == "y" ){
		delete courses.pop();
		console.log(`You have deleted the last course in this array.`)
		console.log(courses);
	}
	else{
		console.log(`You did not delete any course in this array.`)
		console.log(courses);
	}
}

deleteCourse();

//getSingleCourse

let getSingleCourse = (display = prompt(`Which course/ID do you want to display?`)) => {
	let courseIndex = courses.findIndex(course => course.id == display);
	if(courses[courseIndex] !== undefined){
		console.log(courses[courseIndex])
	} else {	
		alert(`Course/ID is not listed in this array.`)
	}
}

getSingleCourse();


//stretchGoal

let stretchGoal = (answer = prompt(`Do you want to display the active courses? y/n`)) => {
	if( answer == "y" ){
		let result = courses.filter(course => course.isActive == true);
		console.log(`You have displayed the active courses below.`)
		console.log(result);
	}
	else{
	console.log(`You chose not to display the active courses.`)
	}
}
stretchGoal();